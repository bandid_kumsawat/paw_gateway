<?php
  @session_start();
  if (isset($_SESSION['username'])){

  }else{
    header("location: ./login/login.html");
  }
?>
<!-- 
ชลจันทร์ พัทยา บีช รีสอร์ท (Cholchan Pattaya Beach Resort)
โรงแรมบางกอกพาเลส (Bangkok Palace Hotel)
โรงแรมเซ็นทาราและคอนเวนชันเซ็นเตอร์ อุดรธานี (Centara Hotel & Convention Centre Udon Thani) 
-->
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>PWA Smart Logger Gateway</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="components/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="dist/css/skins/_all-skins.min.css">
  <!-- jvectormap -->
  <link rel="stylesheet" href="components/jvectormap/jquery-jvectormap.css">
  <!-- Date Picker -->
  <link rel="stylesheet" href="components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="components/bootstrap-daterangepicker/daterangepicker.css">
  <!-- bootstrap wysihtml5 - text editor -->
  <link rel="stylesheet" href="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
  
  <!-- google font -->
  <link href="https://fonts.googleapis.com/css?family=Kanit|Noto+Sans&display=swap" rel="stylesheet">
</head> 

<style>

  * {
    font-family: 'Kanit', sans-serif;
    font-style: Thin;
  }
  body {
    font-family: 'Kanit', sans-serif;
    /* font-size: 16px; */
    background-size: 100%;
    background-repeat: no-repeat;
    /* padding-top: 8%; */
  }
  #map-and-img-1 {
    padding: 0px 10px 0px 0px;
  }
  #map-and-img-2 {
    padding: 0px 0px 0px 10px;
  }
  @media only screen and (max-width: 960px) {
    /* For mobile phones: */
    #map-and-img-1 {
      padding: 0px 0px 10px 0px;
    }
    #map-and-img-2 {
      padding: 10px 0px 0px 0px;
    }
  }


  .switch {
    position: relative;
    display: inline-block;
    width: 60px;
    height: 34px;
  }

  .switch input { 
    opacity: 0;
    width: 0;
    height: 0;
  }

  .slider {
    position: absolute;
    cursor: pointer;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    background-color: #ccc;
    -webkit-transition: .4s;
    transition: .4s;
  }

  .slider:before {
    position: absolute;
    content: "";
    height: 26px;
    width: 26px;
    left: 4px;
    bottom: 4px;
    background-color: white;
    -webkit-transition: .4s;
    transition: .4s;
  }

  input:checked + .slider {
    background-color: #30567f;
  }

  input:focus + .slider {
    box-shadow: 0 0 1px #2196F3;
  }

  input:checked + .slider:before {
    -webkit-transform: translateX(26px);
    -ms-transform: translateX(26px);
    transform: translateX(26px);
  }

  .slider.round {
    border-radius: 34px;
  }

  .slider.round:before {
    border-radius: 50%;
  }


/* Device status active and inactive show color  */
  .active-color {
    color: #2f9605;
    -webkit-animation-name: acive_co; 
    -webkit-animation-duration: 0.5s; 
    animation-name: acive_co;
    animation-duration: 0.5s;
  }

  .inactive-color {
    color: #8d8b8c;
    -webkit-animation-name: inacive_co; 
    -webkit-animation-duration: 0.5s; 
    animation-name: inacive_co;
    animation-duration: 0.5s;
  }
  .box-active {
    color: #000000;
    padding-top : 3px;
    padding-left : 10px;
    padding-right : 10px;
    border-radius : 4px;
    opacity: 0.9;
    background-color: #2f9605;
    -webkit-animation-name: box_atcive_co; 
    -webkit-animation-duration: 0.5s; 
    animation-name: box_atcive_co;
    animation-duration: 0.5s;
  }
  @-webkit-keyframes box_atcive_co {
    from {background-color: #2f9605;}
    to {background-color: #50ac17;}
  }

  @keyframes box_atcive_co {
    from {background-color: #2f9605;}
    to {background-color: #50ac17;}
  }
  .box-inactive {
    color: #000000;
    padding-top : 3px;
    padding-left : 10px;
    padding-right : 10px;
    border-radius : 4px;
    opacity: 0.9;
    background-color: #8d8b8c;
    -webkit-animation-name: box_inatcive_co; 
    -webkit-animation-duration: 0.5s; 
    animation-name: box_inatcive_co;
    animation-duration: 0.5s;
  }
    @-webkit-keyframes box_inatcive_co {
    from {background-color: #aca9aa;}
    to {background-color: #8d8b8c;}
  }

  @keyframes box_inatcive_co {
    from {background-color: #aca9aa;}
    to {background-color: #8d8b8c;}
  }
  @-webkit-keyframes acive_co {
    from {color: #2f9605;}
    to {color: #50ac17;}
  }

  @keyframes acive_co {
    from {color: #2f9605;}
    to {color: #50ac17;}
  }
  @-webkit-keyframes inacive_co {
    from {color: #aca9aa;}
    to {color: #8d8b8c;}
  }

  @keyframes inacive_co {
    from {color: #aca9aa;}
    to {color: #8d8b8c;}
  }

  .pwa-box-1{
    border:solid 1px #13131440;
    border-radius: 3px;
    background-color: #ffffff;
    height:100%;
  }
  .pwa-header-1{
    padding-bottom: 10px;
    padding-top: 10px;
    font-size: 18px;
  }
 /* end  */
  </style>
<body class="sidebar-mini wysihtml5-supported fixed skin-blue">
<div style="font-weight:400;">
  <div class="wrapper">

    <header class="main-header">
      <!-- Logo -->
      <a href="#" class="logo" style = "background-color:#30567f;">
        <span class="logo-mini"><b>PWA</b></span>
        <span class="logo-lg"><b>PWA Smart Logger</b></span>
        <!-- <img src = "./images/logo.png"/ width="120px" height="50px" style = "padding-bottom: 10px;"> -->
      </a>
      <nav class="navbar navbar-static-top" style = "background-color:#30567f;opacity: 10;">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
          <span class="sr-only">Toggle navigation</span>
        </a>

        <div class="navbar-custom-menu">
          <ul class="nav navbar-nav">
            <li class="dropdown user user-menu">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                <img src="images/admin.png " class="user-image" alt="User Image">
                <span class="hidden-xs"><?php echo $_SESSION['email']; ?> [ admin ]</span>
              </a>
              <ul class="dropdown-menu">
                <li class="user-header" style = "background-color:rgb(108, 108, 108);">
                  <img src="images/admin.png" class="img-circle" alt="User Image">
                  <p>
                    <i class="fa fa-user"></i> <?php echo $_SESSION['email']; ?> [admin]
                    <small>[admin]</small>
                  </p>
                </li>
                <li class="user-footer">
                  <div class="pull-left">
                    <!-- <a href="#" class="btn btn-default btn-flat">Profile</a> -->
                  </div>
                  <div class="pull-right">
                    <a href="./login/login.html" class="btn btn-default btn-flat"><i class="fa fa-sign-out"></i> Sign out</a>
                  </div>
                </li>
              </ul>
            </li>
          </ul>
        </div>
      </nav>
    </header>

    <aside class="main-sidebar">
      <section class="sidebar" style = "background-color:#30567f;">
        <ul class="sidebar-menu" data-widget="tree">
          <li class="header">MENU</li>
          <li class='active'>
            <a href="thingboard.php">
              <i class="fa fa-circle"></i> <span style = "font-size:16px;"><strong>Devices</strong></span>
            </a>
          </li>
          <li class="">
            <a href="EMQ.php">
             <i class="fa fa-circle"></i><span style = "font-size:16px;"><strong>MQTT Dashboard</strong></span>
            </a>
          </li>
          <!-- <li class="treeview">
            <a href="#">
              <i class="fa fa-bar-chart"></i>
              <span style = "font-size:16px;"><strong>Statictics</strong></span>
              <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
            </a>
            <ul class="treeview-menu">
              <li><a href="chart.php" style = "font-size:16px;"><i class="fa fa-circle-o"></i> Chart</a></li>
            </ul>
          </li> -->
          <!-- <li class="treeview">
            <a href="#">
              <i class="fa fa-tv"></i>
              <span style = "font-size:16px;"><strong>Project</strong></span>
            </a>
          </li> -->
        </ul>
      </section>
      <!-- /.sidebar -->
    </aside>

    <div class="content-wrapper">





    <section class="content-header">
      <div class='container-fluid'>
        <div class='row'>
          <div class='col-md-12 pwa-box-1'>
            <div class='col-md-12 pwa-header-1'>
              Devices <a href = "http://iotdma.info:60000/dashboard/8a9179f0-1ccb-11ea-92b7-ed0e2123f3d8?publicId=b65e3b00-2076-11ea-bbb4-c9c94d1ab5b1" target="_blank" >[ View Full Screen ]<a>
            </div>

            <hr>

            <div class='col-md-12 pwa-body-1'>
              <iframe src="http://iotdma.info:60000/dashboard/8a9179f0-1ccb-11ea-92b7-ed0e2123f3d8?publicId=b65e3b00-2076-11ea-bbb4-c9c94d1ab5b1" width='100%' height = "1000px" style="border: 0;"></iframe>
            </div>
          </div>
        </div>
      </div>
    </section>


<script src="components/jquery/dist/jquery.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="components/jquery-ui/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.7 -->
<script src="components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Sparkline -->
<script src="components/jquery-sparkline/dist/jquery.sparkline.min.js"></script>
<!-- jvectormap -->
<script src="plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<!-- jQuery Knob Chart -->
<script src="components/jquery-knob/dist/jquery.knob.min.js"></script>
<!-- daterangepicker -->
<script src="components/moment/min/moment.min.js"></script>
<script src="components/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- datepicker -->
<script src="components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<!-- Slimscroll -->
<script src="components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="dist/js/demo.js"></script>

<!-- Google Maps -->
<!-- <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA60weYi_4Yrt6C6GEiiiUegYnPBfWXaBk&callback=initMap" async defer></script> -->
<!-- underscore javascript -->
<script src="dist/js/underscore-min.js"></script>
<!-- moment datetime -->
<script src = "dist/js/moment.js"></script>



<script type="text/javascript" src="dist/js/underscore-min.js"></script>
<script language="JavaScript" >

  $( document ).ready(function() {

  });

</script>
</body>
</html>
