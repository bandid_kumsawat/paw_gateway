<?php
    $dateTime = new DateTime();
    $date_jwt = new DateTime();
    @session_start();
    try {
        include("lib/php-jwt-master/src/JWT.php");
        if (isset($_GET['token'])){
            $key = "secret";
            $token = $_GET['token'];
            $data = JWT::decode($token, $key, array('HS256'));
            $_SESSION['username'] = $data->username;
            $_SESSION['exp'] = $data->exp;
            $_SESSION['email'] = $_GET['email'];

            header("location: thingboard.php");

            print_r($data);
        }
    } catch (Exception $e) {
        header("location: login.php");
    }
?>